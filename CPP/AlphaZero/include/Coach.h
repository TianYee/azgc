#include<iostream>
#include<random>
#include<chrono>
#include<deque>
#include<functional>
#include<ctime>
#include<assert.h>
#include<fstream>

#include"MCTS.h"
#include"Args.h"
#include"NetWrapper.h"

#include "json.hpp"
using json = nlohmann::json;

using namespace std;

class Coach{
    public:
      Coach();
      //Generate training data for one self-play game
      vector < tuple<vector<vector<int>>, vector<float>, int> > ExecuteEpisode();

      void learn();

      //Save training data in json formate
      void saveTrainingData();   

    private:
      Game game;
      mt19937 randomGenerator;
      Args args;
      
      NetWrapper netWrapper;
      
      //Choose a move according to the Pi from MCTS
      int randomChoosePi(vector<float> pi);

      float totalSteps;
      int iteration;

      //traning data
      deque< vector < tuple<vector<vector<int>>, vector<float>, int> >  > itersData;
       
};

Coach::Coach(){
    iteration = 1;
    randomGenerator = mt19937(chrono::high_resolution_clock::now().time_since_epoch().count());
    Args args;    
}



int Coach::randomChoosePi(vector<float> pi){
    uniform_real_distribution<float> dis(0.0f, 1.0f);    
    
    int action = -1;
    float randVal = dis(randomGenerator);
    for (unsigned int i = 0; i < pi.size(); i++){
        if (randVal <= pi[i]){
            action = i;
            break;
        }
        randVal -= pi[i];
    }
    if (action == -1)
        cout << "Error, No action exits in pi prob";
    return action;
}

vector < tuple<vector<vector<int>>, vector<float>, int> > Coach::ExecuteEpisode(){
    float r = 0;
    int steps = 0;
    int curPlayer = 1;
    //If we use bitboard, use game.getInitBitBoard()
    auto board = game.getInitBoard();

    auto predictFunction = bind(&NetWrapper::predict, &netWrapper, placeholders::_1);
    MCTS mcts(predictFunction, args.numMCTSSims, args.Cpuct);

    //Record history. format: vector< tuple<Board, pi, z> > 
    vector < tuple<vector<vector<int>>, vector<float>, int> > history;

    while(true){
        //game.display(board);                
        steps += 1;
        auto canonicalBoard = game.getCanonicalForm(board, curPlayer);
        //tempature
        float temp = args.tempThreshold < steps ? 0 : 1;

        auto pi = mcts.getActionProbs(canonicalBoard, curPlayer, temp);        
        
        //argumented data and push into history
        auto sym = game.getSymmetries(canonicalBoard, pi);
        for(auto data: sym){
            history.push_back(make_tuple(data.first, data.second, curPlayer));
        }

        //random choose a action by pi probability
        int action = randomChoosePi(pi);        
        
        auto next = game.getNextState(board, curPlayer, action);
        board = next.first;
        curPlayer = next.second;

        r = game.getGameEnded(board, curPlayer);
        if(r != 0){
            totalSteps += steps;
            for (unsigned int i = 0; i < history.size();i++){
                history[i] = make_tuple(get<0>(history[i]), get<1>(history[i]), r * (pow(-1, get<2>(history[i]) != curPlayer) ));
            }
            return history;
        }
    }
}

void Coach::saveTrainingData(){
    json iters;
    for(auto eps: itersData){
        json ep;
        for(auto s: eps){
            //Record training data in [(board, pi, z)] format
            json tuple;
            json board;
            for(auto row: get<0>(s)){
                board.push_back(row);
            }            
            tuple.push_back(board); 

            tuple.push_back(get<1>(s));

            tuple.push_back(get<2>(s));
            ep.push_back(tuple);            
        }
        iters.push_back(ep);
    }
    ofstream o("data.json");
    o << iters << std::endl;

    cout << "\n---C++ save data success---\n";
}


void Coach::learn(){    

    for (int i = 1; i <= args.numIters; i++){
        cout << "------C++ start self-play------\n";        

        // record time for each game
        clock_t start, end;
        double cpu_time_used = 0;
        double lastOneTime = 0;
        totalSteps = 0;

        vector <  tuple<vector<vector<int>>, vector<float>, int>  > episodesData;

        for (int j = 1; j <= args.numEps; j++){
            start = clock();
            
            //start one self-play game
            auto oneGameData = ExecuteEpisode();
            
            for (auto data : oneGameData){                
                episodesData.push_back(data);
            }

            //record time for one game
            end = clock();
            lastOneTime = ((double)(end - start)) / CLOCKS_PER_SEC;
            cpu_time_used += lastOneTime;
            double averageSteps = totalSteps/j;
            printf("\r===Played/Total: %i/%i | time: %.2fs | totalTime: %.2fs | averageSteps: %.2f===",
            j, args.numEps,
            lastOneTime, cpu_time_used,
            averageSteps);
            fflush(stdout);
        }
        ///save one iteration traning data 
        itersData.push_back(episodesData);
            
        saveTrainingData();
        
        iteration++;
    }
}
