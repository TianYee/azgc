#pragma once
#include<iostream>
#include<vector>
#include<tuple>
#include<algorithm>
#include<unordered_map>

using namespace std;


//Define your own bitboard class for new game
class bitboard{
    public:
      bitboard();
      unsigned long black;
      unsigned long white;
};
bitboard::bitboard(){
    black = 0x000000000000FFFF;
    white = 0xFFFF000000000000;
}

//Define your own rule for new game
class Game{
    public:
      Game();      
      pair<int, int> getBoardSize();      
      int getActionSize();

      vector<vector<int>> getInitBoard();
      bitboard getInitBitBoard();

      pair<vector<vector<int>>, int> getNextState(vector<vector<int>> &board, int player, int actionIndex);
      pair<bitboard, int> getNextState(bitboard bBoard, int player, int actionIndex);

      vector<int> getValidsMoves(vector<vector<int>> board, int player);
      vector<int> getValidsMoves(bitboard bBoard, int player);

      float getGameEnded(vector<vector<int>> board, int player);
      float getGameEnded(bitboard bBoard, int player);

      vector<vector<int>> getCanonicalForm(vector<vector<int>> board, int player);
      bitboard getCanonicalForm(bitboard bBoard, int player);

      string stringRepresentation(vector<vector<int>> board);
      string stringRepresentation(bitboard bBoard);

      vector<pair<vector<vector<int>>, vector<float>>> getSymmetries(vector<vector<int>> board, vector<float> pi);
      vector<pair<vector<vector<int>>, vector<float>>> getSymmetries(bitboard bBoard, vector<float> pi);

      void display(vector<vector<int>> board);
      void display(bitboard bBoard);

      //Transform bitboard to vector format for NN input
      //Only using bitboard for input need to implement this function
      vector<vector<int>> transBitboardToVec(bitboard bBoard);

      static vector<int> allValidMoves;
      
    private:
      unordered_map<int, int> hashValidsMoves;
};


vector<int> Game::allValidMoves = {1000, 1001, 1101, 1100, 1102, 1202, 1201, 1203, 1303, 1302, 1304, 1404, 1403, 1405, 1505, 1504, 1506, 1606, 1605, 1607, 1707, 1706, 2010, 2011, 2111, 2110, 2112, 2212, 2211, 2213, 2313, 2312, 2314, 2414, 2413, 2415, 2515, 2514, 2516, 2616, 2615, 2617, 2717, 2716, 3020, 3021, 3121, 3120, 3122, 3222, 3221, 3223, 3323, 3322, 3324, 3424, 3423, 3425, 3525, 3524, 3526, 3626, 3625, 3627, 3727, 3726, 4030, 4031, 4131, 4130, 4132, 4232, 4231, 4233, 4333, 4332, 4334, 4434, 4433, 4435, 4535, 4534, 4536, 4636, 4635, 4637, 4737, 4736, 5040, 5041, 5141, 5140, 5142, 5242, 5241, 5243, 5343, 5342, 5344, 5444, 5443, 5445, 5545, 5544, 5546, 5646, 5645, 5647, 5747, 5746, 6050, 6051, 6151, 6150, 6152, 6252, 6251, 6253, 6353, 6352, 6354, 6454, 6453, 6455, 6555, 6554, 6556, 6656, 6655, 6657, 6757, 6756, 7060, 7061, 7161, 7160, 7162, 7262, 7261, 7263, 7363, 7362, 7364, 7464, 7463, 7465, 7565, 7564, 7566, 7666, 7665, 7667, 7767, 7766};


Game::Game(){
    for(int i =0; i < allValidMoves.size(); i++){
        hashValidsMoves[allValidMoves[i]] = i;
    }
}

pair<int, int> Game::getBoardSize(){
    return {8, 8};
}

int Game::getActionSize(){
    return 154;
}

//InitBoard
vector<vector<int>> Game::getInitBoard(){
    vector<vector<int>> b;
    for (int i = 0; i <= 1;i++){
        b.push_back(vector<int>(8, -1));
    }
    for (int i = 0; i <= 3;i++){
        b.push_back(vector<int>(8, 0));
    }
    for (int i = 0; i <= 1;i++){
        b.push_back(vector<int>(8, 1));
    }
    return b;
}

bitboard Game::getInitBitBoard(){
    bitboard b;   
    return b;
}

//NextSate
pair<vector<vector<int>>, int> Game::getNextState(vector<vector<int>> &board, int player, int actionIndex){
    int a = allValidMoves[actionIndex];

    if (player == 1){
        board[a / 1000][(a / 100)%10] = 0;
        board[(a / 10) % 10][a % 10] = 1;
    }
    else{
        board[7- a / 1000][7-(a / 100)%10] = 0;
        board[7- (a / 10) % 10][7-a % 10] = -1;
    }

    return {board, -player};
}

pair<bitboard, int> Game::getNextState(bitboard bBoard, int player, int actionIndex){
    int a = allValidMoves[actionIndex];

    if (player == 1){        
        //white
        int start =(a/1000)*8+(a/100%10);
        bBoard.white &= ~(1UL << (start));

        int end = ((a/10)%10)*8 + (a%10);        
        bBoard.white |= (1UL << (end));
        //black
        bBoard.black &= ~(1UL << (end));
    }
    else{
        //black
        int start = (7-(a/1000))*8+(7-a/100%10);
        bBoard.black &= ~(1UL << (start));

        int end = (7-(a/10)%10)*8 + (7-a%10);        
        bBoard.black |= (1UL << (end));
        //white
        bBoard.white &= ~(1UL << (end));
    }
    return {bBoard, -player};
}

//Valid Moves
vector<int> Game::getValidsMoves(vector<vector<int>> board, int player){
    vector<int> valids(154, 0);
    
    for (int i = 0; i < 8; i++){
        for (int j = 0; j < 8; j++){
            if(board[i][j] == 1 && (i-1)>=0 && board[i-1][j]==0){
                int validMove = i * 1000 + j * 100 + (i - 1) * 10 + j;
                ptrdiff_t pos = distance(allValidMoves.begin(),
                    find(allValidMoves.begin(), allValidMoves.end(), validMove));
                valids[pos] = 1;
            }
            if(board[i][j] == 1 && (i-1)>=0 && (j-1)>=0 && board[i-1][j-1] != 1){
                int validMove = i * 1000 + j * 100 + (i - 1) * 10 + j-1;
                ptrdiff_t pos = distance(allValidMoves.begin(),
                    find(allValidMoves.begin(), allValidMoves.end(), validMove));
                valids[pos] = 1;
            }
            if(board[i][j] == 1 && (i-1)>=0 && (j+1)<8 && board[i-1][j+1] != 1){
                int validMove = i * 1000 + j * 100 + (i - 1) * 10 + j + 1;
                ptrdiff_t pos = distance(allValidMoves.begin(),
                    find(allValidMoves.begin(),  allValidMoves.end(), validMove));
                valids[pos] = 1;
            }
        }
    }
    return valids;
}

vector<int> Game::getValidsMoves(bitboard bBoard, int player){
    vector<int> valids(154, 0);
    auto w = bBoard.white;
    auto b = bBoard.black;

    auto empty = ~(w|b);
    
    //forward moves
    auto fMoves = (w >> 8) & empty;    
    //north east moves
    auto nEastMoves = (w >> 7) & ~(0x0101010101010101) & ~(w);
    //north west Moves
    auto nWestMoves = (w >> 9) & ~(0x8080808080808080) & ~(w);

    for(int i = 0; i < 64; i++){
        if((fMoves >> i) & 1){
            int validMove = (i/8+1) * 1000 + (i%8) * 100 + (i/8) * 10 + (i%8);
            int pos = hashValidsMoves[validMove];
            valids[pos] = 1;
        }
        
        if((nEastMoves >> i) & 1){
            int validMove = (i/8 + 1) * 1000 + (i%8 - 1) * 100 + (i/8) * 10 + (i%8);
            int pos = hashValidsMoves[validMove];
            valids[pos] = 1;
        }
        
        if((nWestMoves >> i) & 1){
            int validMove = (i/8 + 1) * 1000 + (i%8 + 1) * 100 + (i/8) * 10 + (i%8);
            int pos = hashValidsMoves[validMove];
            valids[pos] = 1;
        }        
    }
    return valids;
}

//GameEnd
float Game::getGameEnded(vector<vector<int>> board, int player){ 
    if (player == 1){
        for (int j = 0; j < 8; j++){
            if (board[7][j] == -1){
                return -1.0;
            }
            if (board[0][j] == 1){
                return 1.0;
            }
        }
        for (int i = 7; i >= 0; i--){
            for (int j = 0; j < 8; j++){
                if (board[i][j] == 1){
                    return 0.0;
                }
            }
        }
    }
    else{
        for (int j = 0; j < 8; j++){
            if (board[0][j] == 1){
                return -1;
            }
            if (board[7][j] == -1){
                return 1;
            }
        }
        for (int i = 0; i < 8; i++){
            for (int j = 0; j < 8; j++){
                if (board[i][j] == -1){
                    return 0.0;
                }
            }
        }
    }
    return -1.0;
}

float Game::getGameEnded(bitboard bBoard, int player){
    auto w = bBoard.white;
    auto b = bBoard.black;

    if (player == 1){
        //check black reach end or if white pieces exist
        if((b & 0xFF00000000000000) or ((w & 0xFFFFFFFFFFFFFFFF) == 0)){
            return -1.0;
        }
        //check if white reach end
        if(w & 0x00000000000000FF){
            return 1.0;
        }
    }
    else{
        //check white reach end or if black pieces exist
        if((w & 0x00000000000000FF) or ((b & 0xFFFFFFFFFFFFFFFF) == 0)){
            return -1.0;
        }
        //check if black reach end
        if(b & 0xFF00000000000000){
            return 1.0;
        }
    }
    return 0.0;
}

//Flip board
vector<vector<int>> Game::getCanonicalForm(vector<vector<int>> board, int player){
    if (player == -1){
        for (int i = 0; i <= 3; i++){
            swap(board[i], board[7-i]);
        }
        for(int i=0; i < 8; i++){
            for(int j = 0; j < 4; j++){
                swap(board[i][j], board[i][7-j]);
            }
        }
        for (int i = 0; i < 8; i++){
            for (int j = 0; j < 8; j++){
                board[i][j] *= -1;
            }
        }
        return board;
    }
    return board;
}

bitboard Game::getCanonicalForm(bitboard bBoard, int player){
    if(player == -1){
        auto w = bBoard.white;
        auto b = bBoard.black;
        unsigned long newW = 0UL;
        unsigned long newB = 0UL;

        for(int i = 0; i < 64; i++){
            newW |= ((w >> i & 1UL) <<(63-i));
            newB |= ((b >> i & 1UL) <<(63-i));
        }
        bBoard.white = newB;
        bBoard.black = newW;
        return bBoard;
    }    
    return bBoard;
}

//Hashing board for save UCT info
string Game::stringRepresentation(vector<vector<int>> board){
    string b = "";
    for(auto r: board){
        for(auto e: r){
            b += to_string(e);
        }
    }
    return b;
}

string Game::stringRepresentation(bitboard bBoard){
    auto w = bBoard.white;
    auto b = bBoard.black;
    string s = "";
    for(int i = 0; i < 64; i++){
        if(w >> i & 1){
            s += "1";
        }
        else if(b >> i & 1){
            s += "-1";
        }
        else{
            s += "0";
        }
    }
    return s;
}

//augmented data for training NN
vector<pair<vector<vector<int>>, vector<float>>> Game::getSymmetries(vector<vector<int>> board, vector<float> pi){
    vector< pair<vector<vector<int>>, vector<float>> > symmetris;    
    symmetris.push_back({board, pi});

    return symmetris;
}

vector<pair<vector<vector<int>>, vector<float>>> Game::getSymmetries(bitboard bBoard, vector<float> pi){
    vector< pair<vector<vector<int>>, vector<float>> > symmetris;   
    vector<vector<int>> board;

    board = transBitboardToVec(bBoard);

    symmetris.push_back({board, pi});
    return symmetris;
}


vector<vector<int>> Game::transBitboardToVec(bitboard bBoard){
    vector<vector<int>> board(8, vector<int>(8, 0));

    auto w = bBoard.white;
    auto b = bBoard.black;

    for(int i = 0; i < 64; i++){
        bool check = false;
        if(w >> i & 1){
            board[i/8][i%8] = 1;
            check = true;
        }
        if(b >> i & 1){
            board[i/8][i%8] = -1;
            if(check){
                cout << "-----Error in transBitboardToVec\n---";
            }
            check = true;
        }
        if(!check){
            board[i/8][i%8] = 0;
        }
    }

    return board;
}



void Game::display(vector<vector<int>> board){  
    cout << "\n----Display Game----\n";
    cout << " | 0| 1| 2| 3| 4| 5| 6| 7|\n";
    cout << "--------------------------\n";
    int i = 0;
    for(auto r: board){
        cout << to_string(i++) <<'|';
        for(auto e: r){
            if(e == 1){
                cout << " O ";
            }
            else if(e == -1){
                cout << " X ";
            }
            else{
                cout << " - ";
            }
        }
        cout << '\n';
    }
}
void Game::display(bitboard bBoard){
    auto w = bBoard.white;
    auto b = bBoard.black;
    for(int i = 0; i < 64; i++){
        if(i%8 == 0)
            cout << "\n";
        if((w >> i)&1){
            cout << " 1";
            if((b >> i)&1)
                cout << "Error";
        }
        else if((b >> i)&1){
            cout << "-1";
            if((w >> i)&1)
                cout << "Error";
        }
        else
            cout << " 0";

    }
}