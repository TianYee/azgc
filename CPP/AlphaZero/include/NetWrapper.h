#pragma once
#include <tensorflow/core/platform/env.h>
#include <tensorflow/core/public/session.h> 
#include <tensorflow/c/c_api.h>
#include <ctime>
#include <iostream> 
#include <assert.h>
#include"Args.h"
#include"Game.h"
using namespace std; 
using namespace tensorflow; 

class NetWrapper{
    public:
      //model saving path
      NetWrapper(const string modelPath = "Cmodel/model.pb"); 

      //predict pi and v
      pair<vector<float>, float> predict(vector<vector<int>> board);

    private:
      Args args;
      Game game;
      Session* session;
      bool _new;
};

NetWrapper::NetWrapper(const string modelPath){
  _new = false;
    

  SessionOptions session_options;
  session_options.config.mutable_gpu_options()->set_allow_growth(true);

  Status status = NewSession(session_options, &session);
 
  string model_path = modelPath;

  //Graph Definition for current model
  GraphDef graphdef; 
   
  //check loading model status
  cout << "---Start loading C Tensorflow model---"<< endl;
  Status status_load = ReadBinaryProto(Env::Default(), model_path, &graphdef);
  if (!status_load.ok()) {
      std::cout << "ERROR: Loading model failed:" << model_path << std::endl;
      std::cout << status_load.ToString() << "\n";
      assert(false);
  }
  cout << "---loading model success ---"<< endl;
  cout << "---Start Creating C Session ---"<< endl;
  Status status_create = session->Create(graphdef);
  if (!status_create.ok()) {
      std::cout << "ERROR: Creating graph in session failed..." << status_create.ToString() << std::endl;
      assert(false);
  }
  cout << "---C Session successfully created---"<< endl;


  //Following is for testing the Session works or not.  
  
  vector<vector<float>> vec;
  
    for (int i = 0; i <= 1;i++){
        vec.push_back(vector<float>(8, -1));
    }
    for (int i = 0; i <= 3;i++){
        vec.push_back(vector<float>(8, 0));
    }
    for (int i = 0; i <= 1;i++){
        vec.push_back(vector<float>(8, 1));
    }

  //Difine the number of NCHW, in this case, 
  //we input one data every time, data including one channel, eight row, eight col
  tensorflow::Tensor input(tensorflow::DT_FLOAT, tensorflow::TensorShape({1, 1, 8, 8})); 
  auto input_map = input.tensor<float, 4>();
  
  //Input format: NCHW
  for (int H = 0; H < 8; H++) {
  for (int W = 0; W < 8; W++) {
      input_map(0, 0, H, W) = vec[H][W];
      }
  }

  //is_training for open batch_normalization or not
  Tensor is_training(DT_BOOL, TensorShape());
  is_training.scalar<bool>()() = false;
  
  //we need to define which node we want to output
  //we recommend use name = v, and name = pi for value output and policy output in Python NetWrapper
  vector<tensorflow::Tensor> outputs;
  vector<string> output_node = {"v/Tanh", "pi"};

  Status status_run = session->Run({{"Placeholder", input}, {"is_training", is_training}}, output_node, {}, &outputs);

  if (!status_run.ok()) {
    cout << status_run.ToString() << "\n";

    //if the model didnt name the value and policy in Python NetWrapper
    //we check the default name
    vector<string> output_node = {"dense_2/Tanh", "Softmax"};
    Status status_run = session->Run({{"Placeholder", input}, {"is_training", is_training}}, output_node, {}, &outputs);
    if (!status_run.ok()) {
        cout << status_run.ToString() << "\n";
    }
    else{
        cout << "Success run old graph \n";
    }

  }else{     
    _new = true;
    cout << "Success run new graph" << "\n";
  }
}

pair<vector<float>, float> NetWrapper::predict(vector<vector<int>> board){
    //Difine the number of NCHW, in this case, 
    //we input one data every time, data including one channel, eight row, eight col
    tensorflow::Tensor input(tensorflow::DT_FLOAT, tensorflow::TensorShape({1, 1, 8, 8})); 
    auto input_map = input.tensor<float, 4>();
    
    //Input format: NCHW
    for (int H = 0; H < 8; H++) {
    for (int W = 0; W < 8; W++) {
        input_map(0, 0, H, W) = board[H][W];
        }
    }

    Tensor is_training(DT_BOOL, TensorShape());
    is_training.scalar<bool>()() = false;
    
    vector<tensorflow::Tensor> outputs;
    if(_new){
        vector<string> output_node = {"v/Tanh", "pi"};
        session->Run({{"Placeholder", input}, {"is_training", is_training}}, output_node, {}, &outputs);
    }
    else{
        vector<string> output_node = {"dense_2/Tanh", "Softmax"};
        session->Run({{"Placeholder", input}, {"is_training", is_training}}, output_node, {}, &outputs);
    }
    

    auto vTensor = outputs[0].scalar<float>();
    float v = vTensor();

    Tensor piTensor = outputs[1];

    vector<float> pi;

    auto pi_map = piTensor.tensor<float, 2>();

    for(int j = 0 ; j < 154 ; j++){
        pi.push_back(pi_map(0, j));
    }

    return make_pair(pi, v);
}