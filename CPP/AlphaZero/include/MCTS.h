#pragma once
#include<iostream>
#include<vector>
#include<tuple>
#include<algorithm>
#include<unordered_map>
#include<cmath>
#include<chrono>
#include<random>
#include<functional>
#include"Game.h"
#include<ctime>

#include"Args.h"

using namespace std;

class MCTS{
    public:
      //Input a predict function from NetWrapper, use this function to get v and pi
      MCTS(const function<pair<vector<float>, float>(vector<vector<int>>)> &f, int numMCTSSims, float Cpuct, bool noise_ = true);

      //Output all valid moves' visited number ratio after numMCTSSims MCTS
      vector<float> getActionProbs(vector<vector<int>> canonicalBoard, int curPlayer, float temp);

      //Expand one Node in MCTS
      float Search(vector<vector<int>> canonicalBoard, int curPlayer);
      
      //Add Drirchlet noise in root
      void addDirichlet(vector<vector<int>> canonicalBoard, float alpha);

      //For bitBoard handling
      vector<float> getActionProbs(bitboard canonicalBoard, int curPlayer, float temp);
      float Search(bitboard canonicalBoard, int curPlayer);
      void addDirichlet(bitboard canonicalBoard, float alpha);

    private:
      Args args;
      //record (s,a) Q for UCT
      unordered_map<string, float> Qsa;

      //record (s,a) visited number for UCT
      unordered_map<string, int> Nsa;

      //record (s) visited number for UCT
      unordered_map<string, int> Ns;

      //record (s) all valid moves distribution from neural network
      unordered_map<string, vector<float>> Ps;

      //record (s) is end or not      
      unordered_map<string, float> Es;

      //record (s) all valid moves      
      unordered_map<string, vector<int>> Vs;

      int numMCTSSim;
      float Cpuct;
      float EPS;
      int actionSize;
      Game game;
      mt19937 randomGenerator;
      function < pair<vector<float>, float>(vector<vector<int>>)> predict;

      //use dirichlet noise or not
      bool noise;      
};



MCTS::MCTS(const function<pair<vector<float>, float>(vector<vector<int>>)> &f, int numMCTSSims_, float Cpuct_, bool noise_ /*True*/){
  predict = f;
  numMCTSSim = numMCTSSims_;
  Cpuct = Cpuct_;
  EPS = 0.00000001f;
  actionSize = game.getActionSize();
  randomGenerator = mt19937(chrono::high_resolution_clock::now().time_since_epoch().count());

  noise = noise_;
}


vector<float> MCTS::getActionProbs(vector<vector<int>> canonicalBoard, int curPlayer, float temp){
    vector<float> probs(actionSize, 0);
    
    if(temp == 0 and noise)
      addDirichlet(canonicalBoard, 0.25);

    for (int i = 0; i < numMCTSSim; i++){
      Search(canonicalBoard, curPlayer);
    }

    //if we use limited time in MCTS
    if(args.useTime){
      double restTime = args.limitedTime-1;
      clock_t totalS, totalE;

      //How many MCTS we expanded in limited time
      int expandedNum;
      while(restTime>0){      
      totalS = clock();
      for (int i = 0; i < numMCTSSim; i++){
        Search(canonicalBoard, curPlayer);
      }

      expandedNum += 1;
      totalE = clock();
      restTime -= ((double(totalE-totalS)/CLOCKS_PER_SEC));
      }
      //cout << "expandedNum: " << (expandedNum*numMCTSSim) <<endl;
    }    

    string s = game.stringRepresentation(canonicalBoard);

    for (int a = 0; a < actionSize; a++){
      string sa = s;
      sa += to_string(a);
      if (Nsa.find(sa) != Nsa.end()){
        probs[a] = Nsa[sa];
      }
    }
    //Find best move(the most visted number) if temparature = 0
    if (temp == 0){
      int bestA = max_element(probs.begin(), probs.end()) - probs.begin();
      vector<float> probs(actionSize, 0);
      probs[bestA] = 1;
      return probs;
    }

    //Find move according to the visited number ratio after MCTS
    //The higer ratio, more likely to be choosen
    int sumProb = 0;
    for (int i = 0; i < actionSize; i++){
      probs[i] = pow(probs[i], (1.0/temp));
      sumProb += probs[i];
    }
    for (int i = 0; i < actionSize; i++){
      probs[i] /= sumProb;
    }
    return probs;
}

vector<float> MCTS::getActionProbs(bitboard canonicalBoard, int curPlayer, float temp){
    vector<float> probs(actionSize, 0);

    if(temp == 0 and noise)
      addDirichlet(canonicalBoard, 0.25);
    
    for (int i = 0; i < numMCTSSim; i++){
      Search(canonicalBoard, curPlayer);
    } 

    //if we use limited time in MCTS
    if(args.useTime){
      double restTime = args.limitedTime-1;
      clock_t totalS, totalE;
      //How many MCTS we expanded in limited time
      int expandedNum;
      while(restTime>0){      
      totalS = clock();//
      for (int i = 0; i < numMCTSSim; i++){
        Search(canonicalBoard, curPlayer);
      }

      expandedNum += 1;
      totalE = clock();
      restTime -= ((double(totalE-totalS)/CLOCKS_PER_SEC));
      }
      //cout << "expandedNum: " << (expandedNum*numMCTSSim) <<endl;
    }

    string s = game.stringRepresentation(canonicalBoard);

    for (int a = 0; a < actionSize; a++){
      string sa = s;
      sa += to_string(a);
      if (Nsa.find(sa) != Nsa.end()){
        probs[a] = Nsa[sa];
      }
    }

    if (temp == 0){
      int bestA = max_element(probs.begin(), probs.end()) - probs.begin();
      vector<float> probs(actionSize, 0);
      probs[bestA] = 1;
      return probs;
    }

    int sumProb = 0;
    for (int i = 0; i < actionSize; i++){
      probs[i] = pow(probs[i], (1.0/temp));
      sumProb += probs[i];
    }
    for (int i = 0; i < actionSize; i++){
      probs[i] /= sumProb;
    }
    return probs;
}

//Do one expand node in MCTS
float MCTS::Search(vector<vector<int>> canonicalBoard, int curPlayer){
  string s = game.stringRepresentation(canonicalBoard);  

  //Check if s is in game end(haven't expanded yet), record this s in Es
  if(Es.find(s) == Es.end()){
    Es[s] = game.getGameEnded(canonicalBoard, 1);
  } 
  //if s is a expanded node, check if s end or not, if yes return result
  if(Es[s] != 0.0){
    return -Es[s];
  }

  //s havn't expanded yet, also not a game end node
  if(Ps.find(s) == Ps.end()){
    //predict v and pi
    auto netPredict = predict(canonicalBoard);    
    
    vector<float> ps = netPredict.first;
    float v = netPredict.second;

    //mask to get valid moves
    auto valids = game.getValidsMoves(canonicalBoard, 1);
    for (int i = 0; i < actionSize; i++){
      ps[i] *= valids[i];
    }
    float sumPs = 0;    
    for(auto n: ps){
      sumPs += n;
    }

    //Normalization
    if(sumPs > 0){
      for (int i = 0; i < actionSize; i++){
        ps[i] /= sumPs;
      }
      Ps[s] = ps;
    }

    else{
      cout << "All valid moves were masked, do workaround.";

      float vsCount = 0;
      for(auto n:valids){
        vsCount += n;
      }

      for (int i = 0; i < actionSize; i++){
        ps[i] = valids[i]/vsCount;
      }
      Ps[s] = ps;
    }

    Vs[s] = valids;
    Ns[s] = 0;
    return -v;
  }

    auto valids = Vs[s];
    float curBest = -100;
    int bestA = -1;
    float u;

    //Choose one move in selection phase according to the UCT
    for (int a = 0; a < game.getActionSize(); a++){
      string sa = s + to_string(a);
      if(valids[a]){
        if(Qsa.find(sa) != Qsa.end())
          u = Qsa[sa] + Cpuct * Ps[s][a] * sqrt(Ns[s]) / (1 + Nsa[sa]);
        else
          u = Cpuct * Ps[s][a] * sqrt(Ns[s] + EPS);
        
        if(u > curBest){
          curBest = u;
          bestA = a;
        }
      }
    }
    auto next = game.getNextState(canonicalBoard, 1, bestA);
    auto nextS = game.getCanonicalForm(next.first, next.second);
   
    //The value of new expanded node
    float v = 0;  
    v = Search(nextS, next.second);

    //update Q and U in UCT
    auto sa = s + to_string(bestA);
    if(Qsa.find(sa) != Qsa.end()){
      Qsa[sa] = (Nsa[sa] * Qsa[sa] + v) / (Nsa[sa] + 1);
      Nsa[sa] += 1;
    }
    else{
      Qsa[sa] = v;
      Nsa[sa] = 1;
    }
    Ns[s] += 1;

    return -v;
}


float MCTS::Search(bitboard canonicalBoard, int curPlayer){
  string s = game.stringRepresentation(canonicalBoard);  

  if(Es.find(s) == Es.end()){
    Es[s] = game.getGameEnded(canonicalBoard, 1);
  }

  if(Es[s] != 0.0){
    return -Es[s];
  }

  if(Ps.find(s) == Ps.end()){
    //we need to transform bitboard to vector format before 
    //predict v and pi
    auto vecBoard = game.transBitboardToVec(canonicalBoard);
    auto netPredict = predict(vecBoard);
    
    vector<float> ps = netPredict.first;
    float v = netPredict.second;

    auto valids = game.getValidsMoves(canonicalBoard, 1);
    for (int i = 0; i < actionSize; i++){
      ps[i] *= valids[i];
    }
    float sumPs = 0;    
    for(auto n: ps){
      sumPs += n;
    }

    if(sumPs > 0){
      for (int i = 0; i < actionSize; i++){
        ps[i] /= sumPs;
      }
      Ps[s] = ps;
    }

    else{
      cout << "All valid moves were masked, do workaround.";

      float vsCount = 0;
      for(auto n:valids){
        vsCount += n;
      }

      for (int i = 0; i < actionSize; i++){
        ps[i] = valids[i]/vsCount;
      }
      Ps[s] = ps;
    }

    Vs[s] = valids;
    Ns[s] = 0;
    return -v;
  }

    auto valids = Vs[s];
    float curBest = -100;
    int bestA = -1;
    float u;

    for (int a = 0; a < game.getActionSize(); a++){
      string sa = s + to_string(a);
      if(valids[a]){
        if(Qsa.find(sa) != Qsa.end())
          u = Qsa[sa] + Cpuct * Ps[s][a] * sqrt(Ns[s]) / (1 + Nsa[sa]);
        else
          u = Cpuct * Ps[s][a] * sqrt(Ns[s] + EPS);
        
        if(u > curBest){
          curBest = u;
          bestA = a;
        }
      }
    }
    auto next = game.getNextState(canonicalBoard, 1, bestA);
    auto nextS = game.getCanonicalForm(next.first, next.second);

    float v = 0;
    v = Search(nextS, next.second);

    auto sa = s + to_string(bestA);
    if(Qsa.find(sa) != Qsa.end()){
      Qsa[sa] = (Nsa[sa] * Qsa[sa] + v) / (Nsa[sa] + 1);
      Nsa[sa] += 1;
    }
    else{
      Qsa[sa] = v;
      Nsa[sa] = 1;
    }
    Ns[s] += 1;

    return -v;
}

void MCTS::addDirichlet(vector<vector<int>> canonicalBoard, float alpha){
  //Generate dirichlet noise
  vector<float> dirichletNoise(actionSize, 0.0f);
  float sum = 0.0f;
  gamma_distribution<float> dis(alpha, 1.0f);

  for (int i = 0; i < actionSize; i++){
    dirichletNoise[i] = dis(randomGenerator);
    sum += dirichletNoise[i];
  }

  string s = game.stringRepresentation(canonicalBoard);

  //root s didnt expand yet
  if(Ps.find(s) == Ps.end()){
    auto netPredict = predict(canonicalBoard);
    vector<float> ps = netPredict.first;

    //add dirichlet noise in pi
    auto valids = game.getValidsMoves(canonicalBoard, 1);
    for (int i = 0; i < actionSize; i++){      
      if(valids[i])
        ps[i] = 0.75*(ps[i]) + 0.25*(dirichletNoise[i]);
      else
        ps[i] = 0;
    }
    float sumPs = 0;    
    for(auto n: ps){
      sumPs += n;
    }

    if(sumPs > 0){
      for (int i = 0; i < actionSize; i++){
        ps[i] /= sumPs;
      }
      Ps[s] = ps;
    }
    
    else{
      cout << "All valid moves were masked, do workaround.";

      float vsCount = 0;
      for(auto n:valids){
        vsCount += n;
      }

      for (int i = 0; i < actionSize; i++){
        ps[i] = valids[i]/vsCount;
      }
      Ps[s] = ps;
    }

    Vs[s] = valids;
    Ns[s] = 0;
  }

  //root s have been expanded, we add dirichlet noise to the record of pi
  else{
    vector<float> ps = Ps[s];

    auto valids = Vs[s];
    for (int i = 0; i < actionSize; i++){      
      if(valids[i])
        ps[i] = 0.75*(ps[i]) + 0.25*(dirichletNoise[i]);
      else
        ps[i] = 0;
    }

    float sumPs = 0;    
    for(auto n: ps){
      sumPs += n;
    }
    
    for (int i = 0; i < actionSize; i++){
      ps[i] /= sumPs;
    }
    Ps[s] = ps;
  }
  
}

void MCTS::addDirichlet(bitboard canonicalBoard, float alpha){
  vector<float> dirichletNoise(actionSize, 0.0f);
  float sum = 0.0f;
  gamma_distribution<float> dis(alpha, 1.0f);

  for (int i = 0; i < actionSize; i++){
    dirichletNoise[i] = dis(randomGenerator);
    sum += dirichletNoise[i];
  }

  string s = game.stringRepresentation(canonicalBoard);

  //s didnt expand yet
  if(Ps.find(s) == Ps.end()){
    auto vecBoard = game.transBitboardToVec(canonicalBoard);
    auto netPredict = predict(vecBoard);

    vector<float> ps = netPredict.first;

    auto valids = game.getValidsMoves(canonicalBoard, 1);
    for (int i = 0; i < actionSize; i++){      
      if(valids[i])
        ps[i] = 0.75*(ps[i]) + 0.25*(dirichletNoise[i]);
      else
        ps[i] = 0;
    }
    float sumPs = 0;    
    for(auto n: ps){
      sumPs += n;
    }

    if(sumPs > 0){
      for (int i = 0; i < actionSize; i++){
        ps[i] /= sumPs;
      }
      Ps[s] = ps;
    }

    else{
      cout << "All valid moves were masked, do workaround.";

      float vsCount = 0;
      for(auto n:valids){
        vsCount += n;
      }

      for (int i = 0; i < actionSize; i++){
        ps[i] = valids[i]/vsCount;
      }
      Ps[s] = ps;
    }

    Vs[s] = valids;
    Ns[s] = 0;
  }
  else{
    vector<float> ps = Ps[s];

    auto valids = Vs[s];
    for (int i = 0; i < actionSize; i++){      
      if(valids[i])
        ps[i] = 0.75*(ps[i]) + 0.25*(dirichletNoise[i]);
      else
        ps[i] = 0;
    }

    float sumPs = 0;    
    for(auto n: ps){
      sumPs += n;
    }
    
    for (int i = 0; i < actionSize; i++){
      ps[i] /= sumPs;
    }
    Ps[s] = ps;
  }
}