#pragma once

#include <string>
#include <tuple>
#include <vector>
#include <iostream>

using namespace std;

class Args{
    public:
      Args();
      int numIters;

      //How many games play in one iteration.
      int numEps;

      //Before tempThreshold steps, we will choose a random move 
      //according to the MCTS visited numbers after this step finishing MCTS.
      int tempThreshold;

      int numMCTSSims;
      float Cpuct;

      //If useTime is true, we expaned limitedTime sceonds in MCTS
      //rarther than expand numMCTSSims times in MCTS for every step.
      bool useTime;
      double limitedTime;
};

Args::Args(){
    numIters = 1;
    numEps = 100;
    tempThreshold = 15;
    numMCTSSims = 100;
    Cpuct = 1.0f;

    useTime = false;
    limitedTime = 5.0f;
}
