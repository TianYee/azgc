#include <iostream>
#include <vector>
#include "Coach.h"

using namespace std;

int main(){
    
    cout << "C++ self-play starting...\n";

    Coach c;
    
    c.learn();
    
    return 0;

}