#include <tuple>
#include <iostream>
#include <chrono>
#include <fstream> 

#include "NetWrapper.h"
#include "Args.h"
#include "MCTS.h"
#include "Player.h"

#include "json.hpp"
using json = nlohmann::json;

using namespace std;

class Arena{
    public:
        //input player1 player2        
        Arena(shared_ptr<Player> &_p1, shared_ptr<Player> &_p2);

        //Generate one game comparing result
        float playGame(bool display);

        //Gemerate num games comparing result
        tuple<int, int, int> playGames(int num, bool display);

        //Record one game history
        void saveRecord(vector<vector<vector<int>>> history);

        //determine who's first, 1 = p1, -1 = p2
        int playerOneFirst;
    private:
        Args args;
        Game game;

        shared_ptr<Player> p1;
        shared_ptr<Player> p2;        
};

Arena::Arena(shared_ptr<Player> &_p1, shared_ptr<Player> &_p2){
    p1 = _p1;
    p2 = _p2;
    playerOneFirst = 1;
}


float Arena::playGame(bool display){
    float r = 0;
    int steps = 0;
    int curPlayer = playerOneFirst;
    auto board = game.getInitBoard();
    int action = -1;    
    vector<vector<vector<int>>> history;

    while(game.getGameEnded(board, curPlayer)==0){
        if(display){
            cout << "\nTurn: " << steps << '\n';
            game.display(board);
        }                    
        steps += 1;
        history.push_back(board);

        auto canonicalBoard = game.getCanonicalForm(board, curPlayer);
        
        if(curPlayer == 1){
            action = p1->move(canonicalBoard, curPlayer);
        }
        else{
            action = p2->move(canonicalBoard, curPlayer);
        }
        if(action == -1){
            if(steps<3){
                cout << "no more undo\n";
                steps -= 1;
            }
            else{
                steps -= 3;
                history.pop_back();
                history.pop_back();
                board = history.back();
                history.pop_back();
                cout << "undoing..\n";
            }
            continue;
        }

        auto next = game.getNextState(board, curPlayer, action);
        board = next.first;
        curPlayer = next.second;
    }
    if(true){
        game.display(board);  
        cout << "Game Over Trun: " << steps << '\n';
        cout << "Result: " << game.getGameEnded(board, 1) << '\n';              
    }
    saveRecord(history);
    return game.getGameEnded(board, 1);        
}

tuple<int, int, int> Arena::playGames(int num, bool display){
    int oneWon = 0;
    int twoWon = 0;
    int draws = 0;
    int eps = 0;

    for(unsigned int i= 0; i < num; i++){

        float gameResult = playGame(display);
        if( gameResult == 1){
            oneWon += 1;
        }
        else if(gameResult == -1){
            twoWon += 1;
        }
        else{
            draws += 1;
        }
        //change first
        playerOneFirst *= -1;

        gameResult = playGame(display);
        if( gameResult == 1){
            oneWon += 1;
        }
        else if(gameResult == -1){
            twoWon += 1;
        }
        else{
            draws += 1;
        }
        //change first
        playerOneFirst *= -1;
        cout << "new vs orig Result: Win " << oneWon << " | lose " << twoWon << endl;
    }

    return make_tuple(oneWon, twoWon, draws);
}

void Arena::saveRecord(vector<vector<vector<int>>> history){
    json oneGameRecord;
    for(auto oneBoard: history){
        json toSaveBoard;
        for(auto row: oneBoard){
            toSaveBoard.push_back(row);
        }
        oneGameRecord.push_back(toSaveBoard);
    }
    ofstream o("Record.json");
    o << oneGameRecord << std::endl;
    cout << "Save Record Success\n";
}