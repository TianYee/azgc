#pragma once
#include <tuple>
#include <iostream>
#include <chrono>

#include "NetWrapper.h"
#include "Args.h"
#include "MCTS.h"
#include "Game.h"

using namespace std;

//We need to inherit this class to input in arena class
class Player{
    public:
        virtual int move(vector<vector<int>> canonicalBoard, int curPlayer){
            cout << "pls implement player";
            return -1;
        };
};

//This player is a computer
class NetPlayer: public Player{
    public:
        NetPlayer(const string modelPath);

        //Choose the most visited number move after MCTS
        int ChooseBest(vector<float> pi);

        //return a move after MCTS
        int move(vector<vector<int>> canonicalBoard, int curPlayer) override;

    private:
        shared_ptr<NetWrapper> net;
        shared_ptr<MCTS> mcts;
        Args args;
        Game game;
};

NetPlayer::NetPlayer(const string modelPath){
    net = make_shared<NetWrapper>(modelPath);
    auto predictFunction1 = bind(&NetWrapper::predict, net, placeholders::_1);
    mcts = make_shared<MCTS>(predictFunction1, args.numMCTSSims, args.Cpuct, false);
}

int NetPlayer::ChooseBest(vector<float> pi){
    int action = -1;
    for (unsigned int i = 0; i < pi.size(); i++){
        if (pi[i] == 1.0){
            return i;
        }
    }
    if (action == -1)
        cout << "Error, No action exits in Net pi prob";
    return action;
}

int NetPlayer::move(vector<vector<int>> canonicalBoard, int curPlayer){
    auto pi = mcts->getActionProbs(canonicalBoard, 1, 0);
    auto action = ChooseBest(pi);
    return action;        
}


//This player is a human
class humanPlayer: public Player{
    public:
        //Input from human
        int move(vector<vector<int>> canonicalBoard, int curPlayer) override;

        vector<vector<int>> flipBoard(vector<vector<int>> board);
        void display(vector<vector<int>> canonicalBoard, int mode);
        vector<int> allValidMoves = {1000, 1001, 1101, 1100, 1102, 1202, 1201, 1203, 1303, 1302, 1304, 1404, 1403, 1405, 1505, 1504, 1506, 1606, 1605, 1607, 1707, 1706, 2010, 2011, 2111, 2110, 2112, 2212, 2211, 2213, 2313, 2312, 2314, 2414, 2413, 2415, 2515, 2514, 2516, 2616, 2615, 2617, 2717, 2716, 3020, 3021, 3121, 3120, 3122, 3222, 3221, 3223, 3323, 3322, 3324, 3424, 3423, 3425, 3525, 3524, 3526, 3626, 3625, 3627, 3727, 3726, 4030, 4031, 4131, 4130, 4132, 4232, 4231, 4233, 4333, 4332, 4334, 4434, 4433, 4435, 4535, 4534, 4536, 4636, 4635, 4637, 4737, 4736, 5040, 5041, 5141, 5140, 5142, 5242, 5241, 5243, 5343, 5342, 5344, 5444, 5443, 5445, 5545, 5544, 5546, 5646, 5645, 5647, 5747, 5746, 6050, 6051, 6151, 6150, 6152, 6252, 6251, 6253, 6353, 6352, 6354, 6454, 6453, 6455, 6555, 6554, 6556, 6656, 6655, 6657, 6757, 6756, 7060, 7061, 7161, 7160, 7162, 7262, 7261, 7263, 7363, 7362, 7364, 7464, 7463, 7465, 7565, 7564, 7566, 7666, 7665, 7667, 7767, 7766};
        Game game;
        int mode = 1;
};

int humanPlayer::move(vector<vector<int>> canonicalBoard, int curPlayer){
    string inputAction;
    int action;    

    while(true){
        display(canonicalBoard, mode);
        cout << "Pls enter one move: (or change, undo)\n";
        cin >> inputAction;

        if(inputAction == "change"){
            mode *= -1;
            cout << "change side\n";
            continue;
        }
        if(inputAction == "undo"){
            return -1;
        }

        try{
            if(mode == -1){
                inputAction = to_string(7-(inputAction[0]-'0')) + to_string(7-(inputAction[1]-'0')) + to_string(7-(inputAction[2]-'0')) + to_string(7-(inputAction[3]-'0'));
            }
            action = distance(allValidMoves.begin(),
                find(allValidMoves.begin(), allValidMoves.end(), (stoi)(inputAction)));
            auto valids = game.getValidsMoves(canonicalBoard, 1);

            if(valids[action] && action < valids.size()){
                break;
            }
            else{
                cout << "Invalid move\n";
            }
        }
        catch(...){
            cout << "Invalid move\n";
        }        
    }    
    return action;
}

vector<vector<int>> humanPlayer::flipBoard(vector<vector<int>> board){
    for (int i = 0; i <= 3; i++){
        swap(board[i], board[7-i]);
    }
    for(int i=0; i < 8; i++){
        for(int j = 0; j < 4; j++){
            swap(board[i][j], board[i][7-j]);
        }
    }    
    return board;
}

void humanPlayer::display(vector<vector<int>> canonicalBoard, int mode){
    if(mode == 1){
        game.display(canonicalBoard);
    }
    else{
        game.display(flipBoard(canonicalBoard));
    }
}