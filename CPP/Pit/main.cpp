#include <iostream>
#include <NetWrapper.h>
#include <MCTS.h>
#include <ctime>

#include "Arena.h"
#include "Player.h"

#include <fstream>      
using namespace std;

int main(int argc,char *argv[]){
    //distplay the board in comparing or not
    bool display = false;

    //human vs AI mode
    if(argc > 1 && strcmp(argv[1], "human")==0){
        cout << "start human pitting\n";    

        NetPlayer netp1("orig/model.pb");    
        humanPlayer hp1;

        Player *pp1 = &hp1;
        Player *pp2 = &netp1;
        
        shared_ptr<Player> p1(pp1);
        shared_ptr<Player> p2(pp2);
    
        Arena arena(p1, p2);        
        string f;
        cout << "Who's frist? (h for human, else for net)\n";
        cin >> f;
        if(f == "h"){
            arena.playerOneFirst = 1;
        }
        else{
            arena.playerOneFirst = -1;
        }

        auto result = arena.playGames(1, display);
        cout << "Result: Win " << get<0>(result) << " | lose " << get<1>(result) << '\n';
    }

    //Two model comparing mode
    else{        
        cout << "start two net pitting...\n";
        NetPlayer np1("new/model.pb");
        NetPlayer np2("orig/model.pb");

        Player *pp1 = &np1;
        Player *pp2 = &np2;        

        shared_ptr<Player> p1(pp1);
        shared_ptr<Player> p2(pp2);

        Arena arena(p1, p2);


        clock_t totalS, totalE;
        totalS = clock();
        
        auto result = arena.playGames(150, display);
        cout << "new vs orig Result: Win " << get<0>(result) << " | lose " << get<1>(result) << '\n';

        totalE = clock();
        double totalTime = ((double)((totalE-totalS)/CLOCKS_PER_SEC));
        cout << "TIME: " << totalTime << endl;
    }   
    
    return 0;
}