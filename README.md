# Alpha Zero General C++ (AZGC)

實現C++版自我對下與模型比較的 [alpha-zero-general](https://github.com/suragnair/alpha-zero-general)

## Requirements
目前只試過在Linux 18.04.2下運行，相關套件使用的版本如下

* Python **3.6**
* GCC **4.8.5**
* CUDA **9.0**
* cuDNN **7.3.1**
* TensorFlow **1.8.0**
* Bazel **0.10.0**
* cmkae **3.10.2**

### CUDA and cuDNN
首先先去官網確認顯卡與驅動版本要能符合TensorFlow，以下使用的是GTX 1060，Driver版本390  
安裝驅動程式及確認版本使用
```
sudo apt install nvidia-390
nvidia-smi
```

去官網下載CUDA(runfile)與cuDNN  
下載的同時因Linux 18.04預設gcc版本過高需要安裝使用gcc-4.8才能支援CUDA
```
sudo apt-get install -y gcc-4.8
sudo apt-get install -y g++-4.8
sudo update-alternatives --remove-all gcc
sudo update-alternatives --remove-all g++
sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-4.8 20
sudo update-alternatives --install /usr/bin/g++ g++ /usr/bin/g++-4.8 20
```
需要更換gcc版本時使用以下兩指令進行切換
```
sudo update-alternatives --config gcc
sudo update-alternatives --config g++
```
確認版本正確
```
gcc -v
g++ -v
```

對於CUDA runfile執行以下指令，安裝過程會詢問相關問題，其中會問到需不需要再裝安裝Driver，選擇NO  
```
sudo sh cuda_版本_linux.run
```
安裝patch時一樣使用以上的指令  
完成後使用
```
sudo vim ~/.bashrc
```
在最後一行加入環境變數
```
export PATH=/usr/local/cuda-9.0/bin${PATH:+:$PATH}}
export LD_LIBRARY_PATH=/usr/local/cuda-9.0/lib64${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}
```
輸入以下儲存退出
```
:wq
```

再對於cuDNN解壓後的資料夾中執行
```
sudo cp cuda/include/cudnn.h /usr/local/cuda/include
sudo cp cuda/lib64/libcudnn* /usr/local/cuda/lib64
sudo chmod a+r /usr/local/cuda/include/cudnn.h /usr/local/cuda/lib64/libcudnn*
```

### C++ TensorFlow
安裝相對應版本的[bazel](https://github.com/bazelbuild/bazel/releases)  
下載[C++ TensorFlow原始碼](https://github.com/tensorflow/tensorflow/releases)  
編譯以及安裝相關套件
```
sudo apt-get install autoconf automake libtool curl
bazel build --config=opt --config=cuda //tensorflow:libtensorflow_cc.so
./tensorflow/contrib/makefile/build_all_linux.sh
```

### Python and TensorFlow
最後則是在安裝[anaconda](https://www.anaconda.com/)後建立Python 3.6環境與TensorFlow
```
source ~/.bashrc
conda create -n P36 python=3.6
source activate P36
conda install tensorflow-gpu==1.8.0
```

### Reference
* [For installing CUDA and cuDNN](https://blog.csdn.net/u010801439/article/details/80483036)
* [For installing C++ Tensorflow](https://blog.csdn.net/qq_31261509/article/details/81666763)

  
  
## How to Build
在新電腦上首次Build需要對`CPP/BT`與`CPP/Pit`資料夾中的`CMakeLists.txt`檔案  
修改以下一行，將C++版本TensorFlow編譯後的路徑進行更新
```
//TENSORFLOW_DIR空格後面改成自己用gcc編譯過的TensorFlow路徑
set(TENSORFLOW_DIR /home/hi/Downloads/Ctensorflow/tensorflow)
```

建立`CPP/BUILD`資料夾，在此資料夾中開啟cmd利用以下指令產生C++自我對下與模型比較執行檔
```
cmake ../
make -j2
```
自我對下`BT_C`跟模型比較`Pit`執行檔會產生在`CPP/exeFile`資料夾中   
 

  
  
## How to use
### Training
複製`CPP/exeFile/BT_C`執行檔至`PY_CPP`資料夾下，即可下指令開始訓練
```
source activate P36 //開啟conda的Python環境，若已開啟可跳過
python main.py
```

在`CPP/AlphaZero/include`中的`Args.h`可以修改各項訓練的超參數，如每步的MCTS展開個數  
修改完後去`CPP/BUILD`下`make -j2`重新編譯，再使用新的執行檔開始訓練  

### Training with pre-trained model
在`PY_CPP/main.py`中的*load_model*設定為*True*，並把模型與訓練資料放入`PY_CPP/models`資料夾中後，一樣使用`python main.py`指令開始訓練  
讀取模型要有`__.path.data-`，`__.pth.tar.index`及`__.pth.tar.mata`檔  
訓練資料是`__.pth.tar.examples`，可以load model不load訓練資料  
__為在`PY_CPP/main.py`中`'load_folder_file'`所設定的名稱，這些檔案在訓練過程中會產生在`PY_CPP/temp`中  
  
***Caution***  
`PY_CPP/models`資料夾中包含`oldNet`與`newNet`資料夾，差異只在於有無對類神經網路的節點作命名，`oldNet`中的沒有，而`newNet`有，目前`PY_CPP/BT`是使用`newNet`的網路架構，若要使用在`oldNet`中已train好的模型，需要把`oldNet/BT`資料夾覆蓋`PY_CPP/BT`，而在C版本的使用上會去確認使用哪一個模型不用另外再改  


### Comparing two models
訓練模型完，可以在`PY_CPP/Cmodel`中找到C++版TensorFlow儲存的.pb模型  
在`CPP/exeFile`中，新增`new`與`orig`資料夾並把欲比較的.pb檔各自放入後下指令
```
./Pit
```
得出來的結果是new模型對於orig模型的勝敗，要修改每步的思考時間或MCTS數一樣去修改`CPP/AlphaZero/include/Args.h`後重新編譯  
### Human play
將模型放入`CPP/exeFile/orig`資料夾
```
./Pit human
```
之後可以輸入h或其他選擇先後手，輸入方式是起點的(row, col)+終點的(row, col)，例如6050，會檢查是否為合法走步  
操作的棋子都是O，若是需要換邊可輸入`change`，悔棋則輸入`undo`  
每次對戰結束後都會在當前資料夾下產生該局對戰的盤面資料`Record.json`檔案，當新的一局遊戲結束後會覆蓋舊的資料  

### Modifing new game
目前實作的遊戲是Breakthrough，新的遊戲更改`CPP`中的`Game.h`各函數即可  
另外，在盤面處理上目前只支援一個二維8×8盤面以及bitboard的類別，若使用多個二維盤面，除了修函式的輸入輸出外，應還需更改Python及C++版`NetWrapper`模組中定義NCHW的C個數  

上述更改完後  
訓練與模型比較，直接使用`BT_C`與`Pit`應即可  
人機對戰則還需自行定義`player.h`中的`humanPlayer`類別  