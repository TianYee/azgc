from Coach import Coach
from BT.BTGame import BTGame as Game
from BT.tensorflow.NNet import NNetWrapper as nn
from utils import *
import os
os.environ["CUDA_VISIBLE_DEVICES"] = "0"
args = dotdict({
    'numIters':50,
    'arenaCompare': 5,

    'checkpoint': './temp/',
    'load_model': False,
    'load_folder_file': ('./models/','best.pth.tar'),
    'numItersForTrainExamplesHistory': 20,    
})

import tensorflow as tf

config = tf.ConfigProto()
config.gpu_options.allow_growth=True
sess = tf.Session(config=config)  

if __name__=="__main__":
    g = Game()
    nnet = nn(g)

    if args.load_model:
        nnet.load_checkpoint(args.load_folder_file[0], args.load_folder_file[1])
    nnet.save_pb()

    c = Coach(g, nnet, args)
    if args.load_model:
        print("Load trainExamples from file")
        c.loadTrainExamples()
    
    c.learn() 
