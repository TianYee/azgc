import sys
sys.path.append('..')
from utils import *

import tensorflow as tf

class OthelloNNet():
    def __init__(self, game, args):
        # game params
        self.board_x, self.board_y = game.getBoardSize()
        self.action_size = game.getActionSize()
        self.args = args

        self.kernel_init = tf.contrib.layers.xavier_initializer()
        self.regularizer = tf.nn.l2_loss

        # Neural Net
        self.graph = tf.Graph()
        with self.graph.as_default():
            self.input_boards = tf.placeholder(tf.float32, shape=[None, self.board_x, self.board_y])
            self.dropout = tf.placeholder(tf.float32)
            self.isTraining = tf.placeholder(tf.bool, name="is_training")
            self.target_pis = tf.placeholder(tf.float32, shape=[None, self.action_size])
            self.target_vs = tf.placeholder(tf.float32, shape=[None])
            self.build_net()
        
            self.merged_summary_op = tf.summary.merge_all()

    def dense(self, inputs, units, activation=None):
        return tf.layers.dense(
            inputs,
            units,
            activation=activation,
            kernel_initializer=self.kernel_init,
            kernel_regularizer=self.regularizer
        )
    
    def conv2d(self, inputs, filters, kernel_size):
        return tf.layers.conv2d(
            inputs,
            filters,
            kernel_size,
            padding='SAME',
            data_format='channels_first',
            kernel_initializer=self.kernel_init,
            kernel_regularizer=self.regularizer,
            use_bias=False
        )

    def batch_norm(self, inputs):
        return tf.layers.batch_normalization(
            inputs,
            axis=1,
            training=self.isTraining
        )

    def residual_block(self, inputs):
        orig = inputs
        net = self.conv2d(inputs, self.args.num_channels, 3)
        net = self.batch_norm(net)
        net = tf.nn.relu(net)
        net = self.conv2d(net, self.args.num_channels, 3)
        net = self.batch_norm(net)
        net += orig
        net = tf.nn.relu(net)
        return net

    def build_net(self):
        x_image = tf.reshape(self.input_boards, [-1, 1, self.board_x, self.board_y])
        vs_flat = tf.reshape(self.target_vs, [-1, 1])
        net = self.conv2d(x_image, self.args.num_channels, 3)
        net = self.batch_norm(net)
        net = tf.nn.relu(net)
        for _ in range(self.args.num_blocks):
            net = self.residual_block(net)
        
        # Policy head
        policy_net = self.conv2d(net, 2, 1)
        policy_net = self.batch_norm(policy_net)
        policy_net = tf.nn.relu(policy_net)
        policy_net = tf.reshape(policy_net, [-1, 2*self.board_x*self.board_y])
        self.pi = self.dense(policy_net, self.action_size)
        self.prob = tf.nn.softmax(self.pi)
        #print(self.prob)

        # Value head
        value_net = self.conv2d(net, 1, 1)
        value_net = self.batch_norm(value_net)
        value_net = tf.nn.relu(value_net)
        value_net = tf.reshape(value_net, [-1, self.board_x*self.board_y])
        value_net = self.dense(value_net, 256, activation=tf.nn.relu)
        self.v = self.dense(value_net, 1, activation=tf.nn.tanh)

        # Loss
        self.loss_pi = tf.losses.softmax_cross_entropy(self.target_pis, self.pi)
        self.loss_v = tf.losses.mean_squared_error(vs_flat, self.v)
        self.loss_l2 = tf.losses.get_regularization_loss()
        self.total_loss = self.loss_pi + self.loss_v + 0.0001*self.loss_l2
        #print([n.name for n in tf.get_default_graph().as_graph_def().node])
        tf.summary.scalar('loss_pi', self.loss_pi)
        tf.summary.scalar('loss_v', self.loss_v)

        # Optimizer
        optimizer = tf.train.AdamOptimizer(self.args.lr)
        update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
        with tf.control_dependencies(update_ops):
            self.train_step = optimizer.minimize(self.total_loss)
