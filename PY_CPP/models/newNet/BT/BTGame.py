from .BTLogic import Board

import numpy as np

move = [(-1,0),(-1,-1),(-1,1)]
all_valid_move = ['1000', '1001', '1101', '1100', '1102', '1202', '1201', '1203', '1303', '1302', '1304', '1404', '1403', '1405', '1505', '1504', '1506', '1606', '1605', '1607', '1707', '1706', '2010', '2011', '2111', '2110', '2112', '2212', '2211', '2213', '2313', '2312', '2314', '2414', '2413', '2415', '2515', '2514', '2516', '2616', '2615', '2617', '2717', '2716', '3020', '3021', '3121', '3120', '3122', '3222', '3221', '3223', '3323', '3322', '3324', '3424', '3423', '3425', '3525', '3524', '3526', '3626', '3625', '3627', '3727', '3726', '4030', '4031', '4131', '4130', '4132', '4232', '4231', '4233', '4333', '4332', '4334', '4434', '4433', '4435', '4535', '4534', '4536', '4636', '4635', '4637', '4737', '4736', '5040', '5041', '5141', '5140', '5142', '5242', '5241', '5243', '5343', '5342', '5344', '5444', '5443', '5445', '5545', '5544', '5546', '5646', '5645', '5647', '5747', '5746', '6050', '6051', '6151', '6150', '6152', '6252', '6251', '6253', '6353', '6352', '6354', '6454', '6453', '6455', '6555', '6554', '6556', '6656', '6655', '6657', '6757', '6756', '7060', '7061', '7161', '7160', '7162', '7262', '7261', '7263', '7363', '7362', '7364', '7464', '7463', '7465', '7565', '7564', '7566', '7666', '7665', '7667', '7767', '7766']


class BTGame():
    def __init__(self):
        self.n = 8
        
    def getInitBoard(self):
        b = Board()
        return np.array(b.pieces)

    def getBoardSize(self):
        return (self.n, self.n)

    def getActionSize(self):
        return 154

    def getNextState(self, board, player, action_index):
        b = Board()
        b.pieces = np.copy(board)
        action = all_valid_move[action_index]
        b.pieces[int(action[0])][int(action[1])], b.pieces[int(action[2])][int(action[3])] = 0, 1
        if player == -1:
            return (np.rot90(b.pieces, 2)*-1, -player)
        return (b.pieces, -player)

    def getValidMoves(self, board, player):
        valids = [0]*154
        b = Board()
        b.pieces = np.copy(board)
        i, j = 0, 0
        for i in range(8):
            for j in range(8):
                if b.pieces[i][j] == 1 and i-1 >= 0 and b.pieces[i-1][j] == 0:      ##for move forward
                    valid_move = str(i)+str(j)+str(i-1)+str(j)
                    valids[all_valid_move.index(valid_move)] = 1
                if b.pieces[i][j] == 1 and i-1 >= 0 and j-1 >= 0 and b.pieces[i-1][j-1] != 1:
                    valid_move = str(i)+str(j)+str(i-1)+str(j-1)
                    valids[all_valid_move.index(valid_move)] = 1
                if b.pieces[i][j] == 1 and i-1 >=0 and j+1 < 8 and b.pieces[i-1][j+1] != 1:
                    valid_move = str(i)+str(j)+str(i-1)+str(j+1)
                    valids[all_valid_move.index(valid_move)] = 1
        return np.array(valids)

    def getGameEnded(self, board, player):
        b = Board()
        b.pieces = np.copy(board)
        
        if player == 1:
            for i in range(1, 2):
                for j in range(8):
                    if b.pieces[-i][j] == -1:
                        return -1
            for i in range(8):
                for j in range(8):
                    if b.pieces[-i][j] == 1:
                        return 0
        else:
            for i in range(1):
                for j in range(8):
                    if b.pieces[i][j] == 1:
                        return -1
            for i in range(8):
                for j in range(8):
                    if b.pieces[-i][j] == -1:
                        return 0
        return -1

    def getCanonicalForm(self, board, player):    
        if player == -1:
            return np.rot90(board, 2) * - 1
        return board    
    
    def getSymmetries(self, board, pi):
        """
        Input:
            board: current board
            pi: policy vector of size self.getActionSize()

        Returns:
            symmForms: a list of [(board,pi)] where each tuple is a symmetrical
                       form of the board and the corresponding pi vector. This
                       is used when training the neural network from examples.
        """
        return [(board, pi)]

    def stringRepresentation(self, board):
        return board.tostring()
    
def display(board):
    n = board.shape[0]
    print('   ',end='')
    for y in range(n):
        print (' '+str(y) + "|",end="")
    print("")
    print("   ------------------------")
    for y in range(n):
        print(y, "|",end="")    # print the row #
        for x in range(n):
            piece = board[y][x]    # get the piece to print
            if piece == -1: print("-1 ",end="")
            elif piece == 1: print(" 1",end=" ")
            else:
                if x==n:
                    print(" 0",end=" ")
                else:
                    print(" 0",end=" ")
        print("|")

    print("   -----------------------")