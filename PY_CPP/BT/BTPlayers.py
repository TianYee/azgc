import numpy as np

all_valid_move = ['1000', '1001', '1101', '1100', '1102', '1202', '1201', '1203', '1303', '1302', '1304', '1404', '1403', '1405', '1505', '1504', '1506', '1606', '1605', '1607', '1707', '1706', '2010', '2011', '2111', '2110', '2112', '2212', '2211', '2213', '2313', '2312', '2314', '2414', '2413', '2415', '2515', '2514', '2516', '2616', '2615', '2617', '2717', '2716', '3020', '3021', '3121', '3120', '3122', '3222', '3221', '3223', '3323', '3322', '3324', '3424', '3423', '3425', '3525', '3524', '3526', '3626', '3625', '3627', '3727', '3726', '4030', '4031', '4131', '4130', '4132', '4232', '4231', '4233', '4333', '4332', '4334', '4434', '4433', '4435', '4535', '4534', '4536', '4636', '4635', '4637', '4737', '4736', '5040', '5041', '5141', '5140', '5142', '5242', '5241', '5243', '5343', '5342', '5344', '5444', '5443', '5445', '5545', '5544', '5546', '5646', '5645', '5647', '5747', '5746', '6050', '6051', '6151', '6150', '6152', '6252', '6251', '6253', '6353', '6352', '6354', '6454', '6453', '6455', '6555', '6554', '6556', '6656', '6655', '6657', '6757', '6756', '7060', '7061', '7161', '7160', '7162', '7262', '7261', '7263', '7363', '7362', '7364', '7464', '7463', '7465', '7565', '7564', '7566', '7666', '7665', '7667', '7767', '7766']


class RandomPlayer():
    def __init__(self, game):
        self.game = game

    def play(self, board):
        a = np.random.randint(self.game.getActionSize())
        valids = self.game.getValidMoves(board, 1)
        while valids[a]!=1:
            a = np.random.randint(self.game.getActionSize())
        return a


class HumanOthelloPlayer():
    def __init__(self, game):
        self.game = game

    def play(self, board):
        # display(board)
        valid = self.game.getValidMoves(board, 1)
        for i in range(len(valid)):
            if valid[i]:
                print(all_valid_move[i], end = '-')
                
        while True:            
            print()###
            a = input('Input your action:')
            try:
                a = all_valid_move.index(a)
                if valid[a]:
                    break
                else:
                    print('Invalid')
            except:
                print('Invalid')                      ###
        return a


class GreedyOthelloPlayer():
    def __init__(self, game):
        self.game = game

    def play(self, board):
        valids = self.game.getValidMoves(board, 1)
        candidates = []
        for a in range(self.game.getActionSize()):
            if valids[a]==0:
                continue
            nextBoard, _ = self.game.getNextState(board, 1, a)
            score = self.game.getScore(nextBoard, 1)
            candidates += [(-score, a)]
        candidates.sort()
        return candidates[0][1]
