from collections import deque
import numpy as np
from pytorch_classification.utils import Bar, AverageMeter
import time, os, sys
from pickle import Pickler, Unpickler
from random import shuffle
import time
import json
class Coach():
    """
    This class executes the self-play + learning. It uses the functions defined
    in Game and NeuralNet. args are specified in main.py.
    """
    def __init__(self, game, nnet, args):
        self.game = game
        self.nnet = nnet
        self.pnet = self.nnet.__class__(self.game)  # the competitor network
        self.args = args
        self.trainExamplesHistory = []    # history of examples from args.numItersForTrainExamplesHistory latest iterations
        self.skipFirstSelfPlay = False # can be overriden in loadTrainExamples()
        
    def executeEpisode(self):
        os.system("./BT_C")

    def learn(self):
        """
        Performs numIters iterations with numEps episodes of self-play in each
        iteration. After every iteration, it retrains neural network with
        examples in trainExamples (which has a maximium length of maxlenofQueue).
        It then pits the new neural network against the old one and accepts it
        only if it wins >= updateThreshold fraction of games.
        """
        #Save C model after network initiailizing or loading
        self.nnet.save_checkpoint(folder=self.args.checkpoint, filename='best.pth.tar')
        self.nnet.save_pb()
        

        for i in range(1, self.args.numIters+1):
            print('------ITER ' + str(i) + '------')
            self.executeEpisode()

            # load the C++ training examples to the history 
            self.jsonFileLoad()            
                
            if len(self.trainExamplesHistory) > self.args.numItersForTrainExamplesHistory:
                print("len(trainExamplesHistory) =", len(self.trainExamplesHistory), " => remove the oldest trainExamples")
                self.trainExamplesHistory.pop(0)

            # backup history to a file 
            self.saveTrainExamples(i-1)
            
            # shuffle examlpes before training
            trainExamples = []
            for e in self.trainExamplesHistory:
                trainExamples.extend(e)
            shuffle(trainExamples)
            
            # Train Network
            self.nnet.train(trainExamples)
            
            # Save C model after training
            self.nnet.save_checkpoint(folder=self.args.checkpoint, filename='best.pth.tar')
            self.nnet.save_pb()


    def getCheckpointFile(self, iteration):
        return 'checkpoint_' + str(iteration) + '.pth.tar'

    def saveTrainExamples(self, iteration):
        folder = self.args.checkpoint
        if not os.path.exists(folder):
            os.makedirs(folder)
        filename = os.path.join(folder, self.getCheckpointFile(iteration)+".examples")
        with open(filename, "wb+") as f:
            Pickler(f).dump(self.trainExamplesHistory)
        f.closed

    def loadTrainExamples(self):
        modelFile = os.path.join(self.args.load_folder_file[0], self.args.load_folder_file[1])
        examplesFile = modelFile+".examples"
        if not os.path.isfile(examplesFile):
            print(examplesFile)
            r = input("File with trainExamples not found. Continue? [y|n]")
            if r != "y":
                sys.exit()
        else:
            print("File with trainExamples found. Read it.")
            with open(examplesFile, "rb") as f:
                self.trainExamplesHistory = Unpickler(f).load()
            f.closed
            # examples based on the model were already collected (loaded)
            self.skipFirstSelfPlay = True       
        
    def jsonFileSave(self):
        print("-------------Json File Saving ------------")          

        newData = []
        for eps in self.trainExamplesHistory:    
            newData.append([(s[0].tolist(), s[1], s[2])for s in eps])
        self.trainExamplesHistory = newData
        
        # Writing JSON data
        with open('data.json', 'w') as f:
            json.dump(newData, f)

    def jsonFileLoad(self):
        print("-------------Json File loading ------------")

        with open('data.json', 'r') as f:
            data = json.load(f)

        self.trainExamplesHistory += data
        
        #print(len(self.trainExamplesHistory))# num iter
        #print(len(self.trainExamplesHistory[0]))# num tuple
        #print(len(self.trainExamplesHistory[0][0]))# (board, pi, z)
        print("-------------Json File loading Success------------")
        self.skipFirstSelfPlay = True